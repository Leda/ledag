// vim: expandtab:ts=2:sts=2:sw=2

/*
 * This file is part of ledag.
 *
 * ledag is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ledag is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ledag. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LEDAG_UI_LINK
#define LEDAG_UI_LINK

class GtkGemini;

#include <gtkmm/linkbutton.h>

class Link : public Gtk::LinkButton {
  public:
    Link();
    Link(GtkGemini* gtkGemini, std::string uri, std::string text);

  protected:
    void on_clicked() override;
    bool on_activate_link();

  private:
    GtkGemini* gtkGemini;
};

#endif // LEDAG_UI_LINK
