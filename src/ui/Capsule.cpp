#include "Capsule.hpp"

#include <iostream>

Capsule::Capsule(GtkGemini* gtkGemini, std::string uri, std::string text) : Gtk::TextView(), gtkGemini(gtkGemini), uri(uri), capsule_str(text)
{
    set_monospace();
    set_margin_top(15);
    set_margin_bottom(15);
    set_margin_start(15);
    set_margin_end(15);
    set_tooltip_text(uri);
    set_wrap_mode(Gtk::WrapMode::WRAP_WORD);
    build();
    show();
}

std::string
Capsule::get_uri()
{
    return uri;
}

void
Capsule::build()
{
    Glib::RefPtr<Gtk::TextBuffer> buffer = this->get_buffer();

    buffer->set_text("");

    std::istringstream capsule_stream(capsule_str);
    std::string line;
    bool preformatting = false;
    std::string pretext;

    while (std::getline(capsule_stream, line)) {
        std::string s;
        if (preformatting && line.compare("```") != 0) {
            pretext.append(g_markup_escape_text(line.c_str(), line.length()));
            pretext.append("\n");
        } else if (line.find("# ") == 0 || line.find("## ") == 0 || line.find("### ") == 0) {
            s.append("<span background=\"#FFFFFF\" foreground=\"#0000AA\"><b>");
            s.append(g_markup_escape_text(line.c_str(), line.length()));
            s.append("</b></span>");
            s.append("\n");
            buffer->insert_markup(buffer->end(), s);
        } else if (line.find("* ") == 0) {
            s.append("<span background=\"#FFFFFF\" foreground=\"#aa0000\"><i>");
            s.append(g_markup_escape_text(line.c_str(), line.length()));
            s.append("</i></span>");
            s.append("\n");
            buffer->insert_markup(buffer->end(), s);
        } else if (line.find("=> ") == 0) {
            std::istringstream iss(line);
            std::string item;
            std::getline(iss, item, ' ');
            std::getline(iss, item, '\t');
            std::string link(item);
            std::getline(iss, item, '\t');
            std::string linktext(item);

            Link* mylink = new Link(gtkGemini, link, linktext);
            Glib::RefPtr<Gtk::TextBuffer::ChildAnchor> anchor = buffer->create_child_anchor(buffer->end());
            this->add_child_at_anchor(*mylink, anchor);
            buffer->insert_markup(buffer->end(), "\n");
        } else if (line.compare("```") == 0) {
            preformatting = !preformatting;
            if (preformatting) {
                pretext = "";
                pretext.append(
                    "<span background=\"#808080\" foreground=\"#000000\"><i>");
            } else {
                pretext.append("</i></span>");
                buffer->insert_markup(buffer->end(), pretext);
            }
        } else {
            s.append(line);
            s.append("\n");
            buffer->insert(buffer->end(), s);
        }
    }
}
