#include "Link.hpp"
#include "gtkgemini.hpp"

#include <iostream>
Link::Link() : Gtk::LinkButton()
{
    show();
}

Link::Link(GtkGemini* gtkGemini, std::string uri, std::string text) : Gtk::LinkButton(uri, text), gtkGemini(gtkGemini)
{
    show();
}

void
Link::on_clicked()
{
    Gtk::LinkButton::on_clicked();
}

bool
Link::on_activate_link()
{
    std::string uri = get_uri();
    std::cout << "uri: " << uri << std::endl;
    if (uri.find("http://") == 0) {
        Gtk::LinkButton::on_activate_link();
        return false;
    } else if (uri.find("https://") == 0) {
        Gtk::LinkButton::on_activate_link();
        return false;
    } else if (uri.find("gemini://") == 0) {
        gtkGemini->load_capsule_uri(uri);
        return true;
    }
    return true;
}
