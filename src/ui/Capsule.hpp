// vim: expandtab:ts=2:sts=2:sw=2

/*
 * This file is part of ledag.
 *
 * ledag is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ledag is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ledag. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LEDAG_UI_CAPSULE
#define LEDAG_UI_CAPSULE

#include <gtkmm/textview.h>
#include "Link.hpp"

#include <iostream>
class GtkGemini;

class Capsule : public Gtk::TextView {
  public:
    Capsule(GtkGemini* gtkGemini, std::string uri, std::string capsule);
    std::string get_uri();
    ~Capsule() {}

  protected:
  private:
    GtkGemini* gtkGemini;
    std::string uri;
    std::string capsule_str;
    void build();
};

#endif // LEDAG_UI_CAPSULE
