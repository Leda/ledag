// vim: expandtab:ts=2:sts=2:sw=2

/*
 * This file is part of ledag.
 *
 * ledag is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ledag is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ledag. If not, see <http://www.gnu.org/licenses/>.
 */

#include "gtkgemini.hpp"
#include "connection.hpp"
#include "ui/Capsule.hpp"
#include <map>
#include <iostream>

GtkGemini::GtkGemini() {}

GtkGemini::GtkGemini(Glib::RefPtr<Gtk::Application> app,
                     Glib::RefPtr<Gtk::Builder> builder)
    : app(app), builder(builder)
{
    Gtk::TextView* text = nullptr;
    builder->get_widget<Gtk::TextView>("IntroTextView", text);
    Glib::RefPtr<Gtk::TextBuffer> buffer = text->get_buffer();

    // Load home capsule
    if (load_home_capsule()) {
        Gtk::ScrolledWindow* w = nullptr;
        builder->get_widget<Gtk::ScrolledWindow>("Intro", w);
        w->hide();
    }
}

void
GtkGemini::on_clicked_show_about()
{

    Gtk::AboutDialog* pDialog = nullptr;
    builder->get_widget("About", pDialog);
    pDialog->show();
}
void
GtkGemini::on_clicked_show_bookmarks()
{
    const gchar* user_data_dir = g_get_user_data_dir();
    g_assert(user_data_dir);
    std::string bookmarkfile(user_data_dir);
    bookmarkfile.append("/ledag/bookmarks.gmi");

    gchar* gmi = NULL;
    gsize length = 0;
    GError* error = NULL;
    if (g_file_get_contents(bookmarkfile.c_str(),
                            &gmi,
                            &length,
                            &error)) {
        std::shared_ptr<Capsule> c = std::make_shared<Capsule>(this, bookmarkfile, gmi);
        display_capsule("System:Bookmark", c);

    } else {
        show_error_message(error->message);
    }
}

void
GtkGemini::on_clicked_load_page()
{
    Gtk::Statusbar* statusbar = nullptr;
    builder->get_widget<Gtk::Statusbar>("Statusbar", statusbar);
    Gtk::Entry* url = nullptr;
    builder->get_widget<Gtk::Entry>("UrlEntry", url);
    std::string s = "Loading page... ";
    s.append(url->get_text());
    statusbar->push(s);

    Gtk::TextView* text = nullptr;
    builder->get_widget<Gtk::TextView>("IntroTextView", text);
    Glib::RefPtr<Gtk::TextBuffer> buffer = text->get_buffer();

    Connection connection(url->get_text());
    if (!connection.connect_server()) {
        show_error_message(connection.get_error_message());
        return;
    }
    if (!connection.tls()) {
        show_error_message(connection.get_error_message());
        return;
    }
    if (!connection.send_request()) {
        show_error_message(connection.get_error_message());
    }

    buffer->set_text("");
    std::istringstream f(connection.get_page());
    std::string line;
    int i = 0;

    std::getline(f, line);
    if (line.find("20 text/gemini") == 0) {
        std::shared_ptr<Capsule> capsule = std::make_shared<Capsule>(this, url->get_text(), connection.get_page());

        Gtk::Notebook* notebook = nullptr;
        builder->get_widget<Gtk::Notebook>("Notebook", notebook);
        if (notebook) {
            Gtk::ScrolledWindow* w = new Gtk::ScrolledWindow();
            w->show();
            w->set_policy(Gtk::PolicyType::POLICY_NEVER, Gtk::PolicyType::POLICY_ALWAYS);
            w->add(*capsule);
            notebook->append_page(*w, url->get_text());
        }
    }
}

void
GtkGemini::show_error_message(std::string message)
{
    Gtk::Statusbar* statusbar = nullptr;
    builder->get_widget<Gtk::Statusbar>("Statusbar", statusbar);
    Gtk::Entry* url = nullptr;
    std::string s("Error: ");
    s.append(message);
    statusbar->push(s);
}

void
GtkGemini::display_capsule(std::string name, std::shared_ptr<Capsule> capsule)
{
    Gtk::Notebook* notebook = nullptr;
    builder->get_widget<Gtk::Notebook>("Notebook", notebook);
    if (notebook) {
        if (!tabMap.contains(name)) {
            tabMap.insert(std::pair<std::string, std::shared_ptr<Capsule> >(name, capsule));
            Gtk::ScrolledWindow* w = new Gtk::ScrolledWindow();
            w->show();
            w->set_policy(Gtk::PolicyType::POLICY_NEVER, Gtk::PolicyType::POLICY_ALWAYS);
            w->add(*capsule);
            notebook->append_page(*w, name);
        } else {
            show_error_message("capsule ist schon offen");
        }
    }
}

bool
GtkGemini::load_home_capsule()
{
    const gchar* user_data_dir = g_get_user_data_dir();
    g_assert(user_data_dir);
    std::string intro(user_data_dir);
    intro.append("/ledag/home.gmi");

    gchar* gmi = NULL;
    gsize length = 0;
    GError* error = NULL;
    if (!g_file_get_contents(intro.c_str(),
                             &gmi,
                             &length,
                             &error)) {
        show_error_message(error->message);
        return false;
    }

    home_capsule = new Capsule(this, intro, gmi);

    Gtk::Notebook* notebook = nullptr;
    builder->get_widget<Gtk::Notebook>("Notebook", notebook);
    if (notebook) {
        notebook->append_page(*home_capsule, "Home capsule");
        return true;
    }
    return false;
}

void
GtkGemini::load_capsule_uri(std::string uri)
{
    Connection connection(uri);
    if (!connection.connect_server()) {
        show_error_message(connection.get_error_message());
        return;
    }
    if (!connection.tls()) {
        show_error_message(connection.get_error_message());
        return;
    }
    if (!connection.send_request()) {
        show_error_message(connection.get_error_message());
        return;
    }
    std::shared_ptr<Capsule> c = std::make_shared<Capsule>(this, uri, connection.get_page());
    display_capsule(uri, c);
}
