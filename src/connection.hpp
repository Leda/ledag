// vim: expandtab:ts=2:sts=2:sw=2

/*
 * This file is part of ledag.
 *
 * ledag is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ledag is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ledag. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef CONNECTION_H__
#define CONNECTION_H__

#include "config.hpp"
#include <string>
#include <gnutls/gnutls.h>

class Connection {
  public:
    Connection(std::string request);
    bool connect_server();
    bool tls();
    bool send_request();
    std::string get_page();
    std::string get_error_message();

  private:
    void _parse_url(std::string request);

    std::string request;
    std::string proto;
    std::string host;
    std::string path;

    int fd_socket;
    gnutls_session_t session;

    std::string page;
    std::string error_message;
};

#endif // CONNECTION_H__
