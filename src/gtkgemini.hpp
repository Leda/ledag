// vim: expandtab:ts=2:sts=2:sw=2

/*
 * This file is part of ledag.
 *
 * ledag is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ledag is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ledag. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _GTK_GEMINI_H__
#define _GTK_GEMINI_H__

#include <gtkmm.h>
#include "ui/Link.hpp"
#include "ui/Capsule.hpp"

class GtkGemini {
  public:
    GtkGemini(Glib::RefPtr<Gtk::Application> app,
              Glib::RefPtr<Gtk::Builder> builder);

    void on_clicked_load_page();
    void on_clicked_show_about();
    void on_clicked_show_bookmarks();
    void show_error_message(std::string message);

    void load_capsule_uri(std::string uri);
    void display_capsule(std::string name, std::shared_ptr<Capsule> capsule);

  private:
    GtkGemini();
    Glib::RefPtr<Gtk::Application> app;
    Glib::RefPtr<Gtk::Builder> builder;

    std::map<std::string, std::shared_ptr<Capsule> > tabMap;

    Capsule* home_capsule;

    bool load_home_capsule();
};

#endif // _GTK_GEMINI_H__
