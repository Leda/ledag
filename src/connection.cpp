// vim: expandtab:ts=2:sts=2:sw=2

/*
 * This file is part of ledag.
 *
 * ledag is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ledag is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ledag. If not, see <http://www.gnu.org/licenses/>.
 */

#include "connection.hpp"
#include <assert.h>
#include <errno.h>
#include <iostream>
#include <netdb.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <sys/socket.h>
#include <unistd.h>

#define TCP_IP_PORT       1965
#define URI_SCHEME_GEMINI "gemini"

#define CAFILE        "/etc/ssl/certs/ca-certificates.crt"
#define LEDA_TRUST_DB "trust_hosts.txt"
#define REQ_MAX_LEN   1024

#define CHECK(x) assert((x) >= 0)

#define LOOP_CHECK(rval, cmd)                                         \
    do {                                                              \
        rval = cmd;                                                   \
    } while (rval == GNUTLS_E_AGAIN || rval == GNUTLS_E_INTERRUPTED); \
    assert(rval >= 0)

static int _verify_certificate_callback(gnutls_session_t session);

Connection::Connection(std::string request) : request(request)
{
}

void
Connection::_parse_url(std::string request)
{
    if (!(strstr(request.c_str(), URI_SCHEME_GEMINI) || strstr(request.c_str(), "://"))) {
        return;
    }
    char* url_tmp = strdup(request.c_str());
    proto = std::string(strtok(url_tmp, ":/"));
    host = std::string(strtok(NULL, ":/"));
    if (url_tmp) {
        char* p = strtok(NULL, "");
        if (p)
            path = std::string((p));
        free(url_tmp);
    }
}

bool
Connection::connect_server()
{
    fd_socket = socket(AF_INET, SOCK_STREAM, 0);
    if (fd_socket == -1) {
        error_message = strerror(errno);
        return false;
    }

    _parse_url(request);

    struct hostent* server = gethostbyname(host.c_str());
    if (server == NULL) {
        error_message = hstrerror(h_errno);
        return false;
    }
    struct sockaddr_in serv_addr;

    bzero((char*)&serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy((char*)server->h_addr, (char*)&serv_addr.sin_addr.s_addr,
          server->h_length);
    serv_addr.sin_port = htons(TCP_IP_PORT);
    if (connect(fd_socket, (struct sockaddr*)&serv_addr, sizeof(serv_addr)) < 0) {
        error_message = strerror(errno);
        return false;
    }
    return true;
}

bool
Connection::tls()
{

    gnutls_priority_t priority_cache;
    //    gnutls_global_set_log_function(_tls_log);
    gnutls_global_set_log_level(5);
    CHECK(gnutls_global_init());

    CHECK(gnutls_init(&session, GNUTLS_CLIENT));
    CHECK(gnutls_priority_init(&priority_cache, NULL, NULL));
    CHECK(gnutls_priority_set(session, priority_cache));
    gnutls_handshake_set_timeout(session, GNUTLS_DEFAULT_HANDSHAKE_TIMEOUT);

    gnutls_certificate_credentials_t xcred;
    CHECK(gnutls_certificate_allocate_credentials(&xcred));
    CHECK(gnutls_certificate_set_x509_trust_file(xcred, CAFILE,
                                                 GNUTLS_X509_FMT_PEM));
    gnutls_certificate_set_verify_function(xcred, _verify_certificate_callback);
    CHECK(gnutls_credentials_set(session, GNUTLS_CRD_CERTIFICATE, xcred));

    gnutls_transport_set_int(session, fd_socket);

    int ret = 0;
    do {
        ret = gnutls_handshake(session);
    } while (ret < 0 && gnutls_error_is_fatal(ret) == 0);
    if (ret < 0) {
        if (ret == GNUTLS_E_CERTIFICATE_VERIFICATION_ERROR) {
            gnutls_certificate_type_t type;
            unsigned status;
            gnutls_datum_t out;
            type = gnutls_certificate_type_get(session);
            status = gnutls_session_get_verify_cert_status(session);
            CHECK(
                gnutls_certificate_verification_status_print(status, type, &out, 0));
            gnutls_free(out.data);
        }
        error_message = gnutls_strerror(ret);
    } else {
        char buffer[1024], *desc;
        desc = gnutls_session_get_desc(session);
        printf("- Session info: %s\n", desc);
        gnutls_free(desc);
    }

    return true;
}

bool
Connection::send_request()
{
    int ret = 0;
    char buffer_response[1024];
    bzero(buffer_response, 1024);

    std::cout << "Sending request " << request << std::endl;
    std::string r = std::string(request);
    r.append("\n");

    LOOP_CHECK(ret, gnutls_record_send(session, r.c_str(), strlen(r.c_str())));

    do {
        do {
            ret = gnutls_record_recv(session, buffer_response, 1024);
        } while (ret == GNUTLS_E_AGAIN || ret == GNUTLS_E_INTERRUPTED);
        page.append(buffer_response, ret);
    } while (ret > 0);

    if (ret == 0) {
        std::cout << "- Peer has closed the TLS connection" << std::endl;
    } else if (ret < 0 && gnutls_error_is_fatal(ret) == 0) {
        std::cout << "Warning: " << gnutls_strerror(ret) << std::endl;
    } else if (ret < 0) {
        error_message = gnutls_strerror(ret);
        return false;
    }
    gnutls_bye(session, GNUTLS_SHUT_RDWR);
    close(fd_socket);
    return true;
}

std::string
Connection::get_page()
{
    return page;
}

static int
_verify_certificate_callback(gnutls_session_t session)
{
    return 0;
}

std::string
Connection::get_error_message()
{
    return error_message;
}
