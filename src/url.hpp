// vim: expandtab:ts=2:sts=2:sw=2

/*
 * This file is part of ledag.
 *
 * ledag is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ledag is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ledag. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef URL_H__
#define URL_H__

#include "config.hpp"
#include <string>
#include <vector>

/**
 * URL
 */
class URL {
  public:
    URL(std::string);
    std::string url();

  private:
    std::string raw;
    std::string proto;
    std::string user;
    std::string pass;
    std::string host;
    std::string port;
    std::string path;
    std::vector<std::string> paths;
};

#endif // URL_H__
